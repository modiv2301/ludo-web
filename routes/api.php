<?php

use App\Http\Controllers\Api\V1\Customer\CTestController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/v1/test', 'Api\V1\TestController@test');

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['middleware' => ['optimizeImages'], 'prefix' => '/v1/customer', 'namespace' => 'Api\V1\Customer'], function () {
    Route::get('/test', [CTestController::class, 'test']);

    // -------- Register And Login API ----------
    Route::controller(CAuthController::class)->group(function () {
        Route::post('login', 'login');
        Route::post('new_register', 'new_register');
        // Route::post('register', 'register');
        
    });

    // -------- Register And Login API ----------
    Route::group(['middleware' => ['jwt.auth']], function () {
        /* logout APi */
        Route::controller(CAuthController::class)->group(function () {
            Route::post('allgame', 'allgame');
        Route::post('allbattle', 'allbattle');
        Route::post('wallet', 'wallet');
        Route::post('wallet_history', 'wallet_history');
        Route::post('game_result', 'game_result');
        Route::post('lossgame', 'lossgame');
        Route::post('game_history', 'game_history');
        Route::post('find_room', 'find_room');
        Route::post('battle_use_room', 'battle_use_room');
        Route::post('user_data', 'user_data');
            Route::post('logout', 'logout');
        });

        /* Profile Controller */
        Route::controller(CProfileController::class)->group(function () {
            /*Profile API */
            Route::get('profile', 'profile');
            Route::put('update-profile', 'updateProfile');
            Route::post('update-profile-image', 'updateProfileImage');
        });
    });
});