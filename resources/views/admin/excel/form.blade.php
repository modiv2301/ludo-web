<!--begin::Card body-->
<div class="card-body">

    <!--begin::Input group-->
    <div class="row mb-6">
        <!-- <label class="col-lg-2 col-form-label required fw-bold fs-6">Battle Name</label>
        
        <div class="col-lg-4 fv-row">
       
            <input type="text"  class="form-control form-control-lg form-control-solid" name="battle_id" placeholder="Battle Name">
        </div> -->
       
        <label class="col-lg-2 col-form-label required fw-bold fs-6">File</label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-4 fv-row">
            <!-- {!! Form::text('room_no', null, ['placeholder' => trans_choice('content.room_no_title', 1), 'value' => 'Max', 'class' => 'form-control form-control-lg form-control-solid mb-3 mb-lg-0']) !!} -->
            <input type="file" class="form-control form-control-lg form-control-solid" name="excel_file">
        </div>

        
        <!--end::Col-->
    </div>
    <div class="row mb-6">

        <!-- <label class="col-lg-2 col-form-label fw-bold fs-6">
            <span class="required">Room Code</span>
            
        </label> -->
        <!--end::Label-->
        <!--begin::Col-->
        <!-- <div class="col-lg-4 fv-row">
            -->
        <!-- {!! Form::number('prize', null, ['placeholder' => trans_choice('content.prize_title', 1), 'class' => 'form-control form-control-lg form-control-solid']) !!} -->
        <!-- <input type="number" name="room_code" class="form-control form-control-lg form-control-solid" placeholder="Room code">  
        </div> -->
        <!--end::Col-->
    </div>
    <!--end::Input group-->

    <!--begin::Input group-->
    
    <!--end::Input group-->

    <!--begin::Input group-->

    <!--end::Input group-->
   

</div>
<!--end::Card body-->

@push('scripts')
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js') }}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\Admin\winningRequest', 'form') !!}
@endpush
