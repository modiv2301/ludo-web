<!--begin::Card body-->
<div class="card-body">

    <!--begin::Input group-->
    <div class="row mb-6">
        <!--begin::Label-->
        <!-- <label class="col-lg-2 col-form-label required fw-bold fs-6">{{ trans_choice('content.total_player_title', 1) }}</label> -->
        <label class="col-lg-2 col-form-label required fw-bold fs-6">Game Name</label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-4 fv-row">
            <!-- {!! Form::number('total_player', null, ['min' => 2, 'max' => 6, 'value' => 2, 'class' => 'form-control form-control-lg form-control-solid', 'placeholder' => trans_choice('content.total_player_title', 1)]) !!} -->
            <input type="text"  class="form-control form-control-lg form-control-solid" name="game_name" placeholder="Game Name">
        </div>
        <!--end::Col-->

        <!--begin::Label-->
        <!-- <label class="col-lg-2 col-form-label required fw-bold fs-6">{{ trans_choice('content.room_no_title', 1) }}</label> -->
        <label class="col-lg-2 col-form-label required fw-bold fs-6">Banner Image</label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-4 fv-row">
            <!-- {!! Form::text('room_no', null, ['placeholder' => trans_choice('content.room_no_title', 1), 'value' => 'Max', 'class' => 'form-control form-control-lg form-control-solid mb-3 mb-lg-0']) !!} -->
            <input type="file" class="form-control form-control-lg form-control-solid" name="banner_image">
        </div>
        <!--end::Col-->
    </div>
    <!--end::Input group-->

    <!--begin::Input group-->
    <div class="row mb-6">
        <!--begin::Label-->
        <label class="col-lg-2 col-form-label fw-bold fs-6">
            <!-- <span class="required">{{ trans_choice('content.entry_fee_title', 1) }}</span> -->
           <span class="required">No Of Player</span>
            <!-- <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip"
                title="Entry Fee should be atleast 1."></i> -->
        </label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-4 fv-row">
            <!-- {!! Form::number('entry_fee', null, ['placeholder' => trans_choice('content.entry_fee_title', 1), 'class' => 'form-control form-control-lg form-control-solid']) !!} -->
            <input type="number" name="no_player" class="form-control form-control-lg form-control-solid" placeholder="No of Player">  
        </div>
        <!--end::Col-->

        <!--begin::Label-->
        <label class="col-lg-2 col-form-label fw-bold fs-6">
            <span class="required">No of Battle</span>
            <!-- <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip"
                title="Prize should be atleast 1."></i> -->
        </label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-4 fv-row">
           
        <!-- {!! Form::number('prize', null, ['placeholder' => trans_choice('content.prize_title', 1), 'class' => 'form-control form-control-lg form-control-solid']) !!} -->
        <input type="number" name="no_battle" class="form-control form-control-lg form-control-solid" placeholder="No of Battle">  
        </div>
        <!--end::Col-->
    </div>
    <!--end::Input group-->

    <!--begin::Input group-->
    <div class="row mb-6">
        <!--begin::Label-->
        <label class="col-lg-2 col-form-label fw-bold fs-6">
            <span class="required">Entry Amount</span>
        </label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-4 fv-row">
            <!-- {!! Form::text('start_time', null, ['placeholder' => trans_choice('content.start_time_title', 1), 'class' => 'form-control form-control-lg form-control-solid datetimepicker']) !!} -->
            <input type="number" name="entry_amount" class="form-control form-control-lg form-control-solid" placeholder="Entry Amount">  
        </div>
        <!--end::Col-->

        <label class="col-lg-2 col-form-label required fw-bold fs-6">Winning Amount</label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-4 fv-row">
            <!-- {!! Form::text('room_no', null, ['placeholder' => trans_choice('content.room_no_title', 1), 'value' => 'Max', 'class' => 'form-control form-control-lg form-control-solid mb-3 mb-lg-0']) !!} -->
            <input type="text" class="form-control form-control-lg form-control-solid" name="win_amount">
        </div>
        
    </div>
    <!--end::Input group-->
    <div class="row mb-6">
        <!--begin::Label-->
        <label class="col-lg-2 col-form-label fw-bold fs-6">
            <span class="required">Trending Game</span>
        </label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-4 fv-row">
            <!-- {!! Form::text('start_time', null, ['placeholder' => trans_choice('content.start_time_title', 1), 'class' => 'form-control form-control-lg form-control-solid datetimepicker']) !!} -->
            <!-- <input type="text" name="trending_game" class="form-control form-control-lg form-control-solid" placeholder="Trending Game">   -->
            yes<input class="form-check-input" type="radio" id="flexSwitchCheckDefault" value="0" name="trending_game">
            no<input class="form-check-input" type="radio" id="flexSwitchCheckDefault" value="1" name="trending_game">
        </div>
        <!--end::Col-->
      
    </div>

</div>
<!--end::Card body-->


