<?php

return [

    'login' =>
    array(
        'customer' => 'Customer',
        'user' => 'User',
        'name' => 'Name',
        'email' => 'Email',
        'phone' => 'Mobile',
        'company_name' => 'Common-Setup',
    ),

    'topbar' =>
    array(
        'language' => 'Language',
        'my_profile' => 'My Profile',
        'dashboard' => 'Dashboard',
        'change_language' => 'Change Language',
    ),

    'setting' =>
    array(
        'edit_setting' => 'Edit Settings',
        'site_name_title' => 'Site Name',
        'copyright_text_title' => 'Copyright Text',
        'logo' => 'Logo',
        'favicon' => 'Favicon',
        'phone_title' => 'Phone Title',
        'site_name_title' => 'Site Name Title',
        'copyright_text_title' => 'Copyright Text Title',
        'site_mode_title' => 'Site Mode Title',
        'home_page_title' => 'Home Page Title',
        'address_title' => 'Address Title',
    ),

    'month' =>
    array(
        'january' => 'January',
        'february' => 'February',
        'march' => 'March',
        'april' => 'April',
        'may' => 'May',
        'june' => 'June',
        'july' => 'July',
        'august' => 'August',
        'september' => 'September',
        'october' => 'October',
        'november' => 'November',
        'december' => 'December',
    ),

    'week' =>
    array(
        'sunday' => 'Sunday',
        'monday' => 'Monday',
        'tuesday' => 'Tuesday',
        'wednesday' => 'Wednesday',
        'thursday' => 'Thursday',
        'friday' => 'Friday',
        'saturday' => 'Saturday',
    ),


    'dashboard_cards' =>
    array(
        'new_users' => 'New Users',
        'total_roles' => 'Total Roles',
        'total_permissions' => 'Total Permissions',
        'select_year' => 'Select Year',
        'total_manager' => 'Total Manager',
        'total_vendors' => 'Total Vendors',
        'weekly_sales' => 'Weekly Sales',
        'yearly_sales' => 'Yearly Sales',

        'item_count' => 'Item Count',
        'total_vendors' => 'Total Vendors',
        'total_income' => 'Total Income',
        'total_purchase' => 'Total Purchase',
        'total_fund_balance' => 'Total Fund Balance',
        'total_transaction' => 'Total Transaction',
        'total_amount_spent' => 'Total Amount Spent',
        'total_profit_loss' => 'Total Profit Loss',
        'total_dept_balance' => 'Total Dept Balance',
        'total_asset' => 'Total Asset',
        'total_unpaid_bill' => 'Total UnPaid Bill',
        'total_unpaid_amount' => 'Total Amount To Be Pay (Vendor & Staff)',
        'total_unrecieved_bill' => 'Total UnRecieved Bill',
        'total_unrecieved_amount' => 'Total Amount To Be Receive (Client)',
        'total_revenue' => 'Total Revenue',
        'total_clients' => 'Total Clients',
    ),


    //SideBar
    'sidebar' => array(
        'dashboard' => 'Dashboard',
        'menu' => 'Menu|Menus',
        'user_management' => 'User Management',
        'section' => 'Section',
        'user' => 'User|Users',
        'role' => 'Role|Roles',
        'permission' => 'Permission|Permissions',
        'customer' => 'Customer|Customers',
        'product' => 'Product|Products',
        'battle' => 'Battle|Battles',
        'game' => 'Game|Games',
        'result' => 'Result|Results',
        'wallethistory' => 'WalletHistory|WalletHistorys',
        'excel' => 'Excel|Excels',
        'winning' => 'Winning|Winnings',
    ),


    //Products Manager
    'products' => array(
        'id' => 'ID',
        'name' => 'Name',
        'status' => 'Status',
        'upload_product_images' => 'Upload Product Images',
        'title_title' => 'Title',
        'quantity_title' => 'Quantity',
        'price_title' => 'Price',
        'import_product' => 'Import Product',
    ),

    //Table Title
    'id_title' => 'ID',
    'name_title' => 'Name|Names',
    'email_title' => 'Email|Emails',
    'phone_title' => 'Phone Numbers|Phone Numbers',
    'description_title' => 'Description|Descriptions',
    'image_title' => 'Image|Images',
    'status_title' => 'Status',
    'role_title' => 'Role|Roles',
    'joined_date_title' => 'Joined Date|Joined Dates',
    'action_title' => 'Action|Actions',
    'create_title' => 'Create',
    'edit_title' => 'Edit',
    'update_title' => 'Update',
    'delete_title' => 'Delete',
    'back_title' => 'Back',
    'number_title' => 'No.',
    'permission_title' => 'Permission',
    'title_title' => 'Title',
    'quantity_title' => 'Quantity',
    'price_title' => 'Price',

    //Form Data
    'please_select' => 'Please Select',
    'profile_details' => 'Profile Details',
    'allow_changes' => 'Allow Changes',
    'yes' => 'Yes',
    'no' => "No",

    'change_password' => 'Change Password',
    'old_password_title' => 'Old Password',
    'password_title' => 'Password',
    'confirm_password_title' => 'Confirm Password',

    'pro' => 'Pro',
    'filter' => 'Filter',
    'filter_options' => 'Filter Options',
    'reset' => 'Reset',
    'active' => 'Active',
    'inactive' => 'Inactive',
    'active_title' => 'Active',
    'inactive_title' => 'Inactive',
    'select_status' => 'Select Status',
    'paid' => 'Paid',
    'unpaid' => 'Unpaid',

    'profile' => 'Profile',
    'edit_profile' => 'Edit Profile',

    'search_title' => 'Search',
    'submit_title' => 'Submit',
    'reset_title' => 'Reset',
    'download_title' => 'Download',
    'export_title' => 'Export',
    'excel_title' => 'Excel',
    'csv_title' => 'Csv',
    'two_step_title' => 'Two-Step',

    'user' => 'User|Users',
    'customer' => 'Customer|Customers',
    'product_history' => 'Product History|Product Histories',
    'customer_product_history' => 'Customer Product History|Customer Product Histories',
    'product' => 'Product|Products',
    'battle' => 'Battle|Battles',
    'game' => 'Game|Games',
    'result' => 'Result|Results',
    'excel' => 'Excel|Excels',
    'winning' => 'Winning|Winnings',

    'user_id' => 'User Id',
    'customer_id' => 'Customer Id',
    'import' => 'Import',
    'import_title' => 'Import',
    'select_file' => 'Select File',
    'download_format_title' => 'Download Format',
    'test_tab' => 'Test Tab',

    'total_player_title' => 'Total Players',
    'room_no_title' => 'Room No.',
    'entry_fee_title' => 'Entry Fee',
    'prize_title' => 'Prize',
    'start_time_title' => 'Start Time',
    'open_title' => 'Open',
    'running_title' => 'Running',
    'is_active_title' => 'Is Active',

    'name' => 'Name',
    'image' => 'Banner Image',
    'no_player' => ' No. Player',
    'no_room' => 'No room',
    'entry_amount' => 'Prize',
    'win_amount' => 'Win amount',
    'trending_game' => 'Is Active',


    'user_id' => 'Name',
    'battle_id' => 'Battle Name',
    'battle_room' => 'Room Code',
    'screenshot' => 'Screenshot',
    'status' => 'Status',
    'change_status' => 'Change Status',

    'wallet_amount' => 'Wallet Amount',
    'user_id' => 'User',
    'transaction_id' => 'Transaction Id',
    
    'status' => 'Status',
    

];
