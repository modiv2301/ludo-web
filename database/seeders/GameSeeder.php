<?php

namespace Database\Seeders;

use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class GameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $game  = [
            [
                 'name' => 'Sureash',
                 'no_player' => '4',
                 'no_room' => '201',
                 'entry_amout' => '50',
                 'win_amount' => '100',
                 'trending_game' => '1'
            ],

            [
                'name' => 'Nikhil', 'no_player' => '4','no_room' => '202','entry_amout' => '50','win_amount' => '100','trending_game' => '1'],
        ];
        Setting::insert($game);
    }
}
