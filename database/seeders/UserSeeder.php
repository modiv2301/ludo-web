<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data =  [
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'mobile_no' => '9876548965',
            'image' => null,
            'is_active' => 1,
            'password' => Hash::make('12345678'),
        ];
        User::create($data);
    }
}