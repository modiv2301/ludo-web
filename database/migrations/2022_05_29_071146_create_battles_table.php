<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * php artisan migrate:refresh --path=database\migrations\2022_05_29_071146_create_battles_table.php
     */
    public function up()
    {
        Schema::create('battles', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('total_player')->length(20)->default(2)->comment('Minimum 2 players.');
            $table->bigInteger('player1_id')->length(20)->nullable();
            $table->bigInteger('player2_id')->length(20)->nullable();
            $table->bigInteger('player3_id')->length(20)->nullable();
            $table->bigInteger('player4_id')->length(20)->nullable();
            $table->bigInteger('player5_id')->length(20)->nullable();
            $table->bigInteger('player6_id')->length(20)->nullable();
            $table->string('room_no', 40)->nullable();
            $table->bigInteger('entry_fee')->length(20)->default(0)->comment('in rs');
            $table->bigInteger('prize')->length(20)->default(0)->comment('in rs');
            $table->timestamp('start_time')->nullable();
            $table->tinyInteger('status')->default(1)->comment('1:Open, 2:Running');
            $table->tinyInteger('is_active')->default(1)->comment('1:Active, 0:Inactive');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('battles');
    }
};
