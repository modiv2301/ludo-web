<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_battles', function (Blueprint $table) {
            $table->id();
            $table->string('name', 150);
            $table->string('image')->nullable();
            $table->bigInteger('players');
            $table->bigInteger('battles');
            $table->bigInteger('amount');
            $table->bigInteger('win_amount');
            $table->tinyInteger('trending_game')->default(0)->comment('0: Trending 1:Trending Not');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_battles');
    }
};
