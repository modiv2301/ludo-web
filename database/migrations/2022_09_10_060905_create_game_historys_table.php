<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_historys', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('room_code_id')->length(20);
            $table->bigInteger('user_id')->length(20);
            $table->bigInteger('game_id')->length(20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_historys');
    }
};
