<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('battle_room_code', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('battle_id')->length(20);
            $table->string('room_code', 200)->nullable();
            $table->tinyInteger('status')->default(0)->comment('0: available 1:not available');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('battle_room_code');
    }
};
