<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('winnings', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('battle_id')->length(20);
            $table->bigInteger('user_id')->length(20);
            $table->tinyInteger('status')->default(1)->comment('1:Win, 2:Lose 3:Cancel');
            $table->string('screenshot', 100)->nullable();
            $table->tinyInteger('is_verify')->default(0)->comment('0:Not-Verified 1:Verified');
            $table->tinyInteger('prize_transfer_status')->default(0)->comment('1:Success 2:Failed 3:Pending');
            $table->bigInteger('amount')->length(20)->nullable()->default(0);

            $table->softDeletes();
            $table->timestamps();

            $table->index('battle_id');
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('winnings');
    }
};
