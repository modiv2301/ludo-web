<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ApiGameResultRequest extends FormRequest
{
    protected $stopOnFirstFailure = true;
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required',
            'battle_id' => 'required',
            'battle_room_code_id' => 'required',
            'status' => 'required',
            'screenshot' => 'required',
            'admin_status' => 'required',
        ];
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'user_id.required' => __('validation.required', ['attribute' => 'User']),

            'battle_id.required' => __('validation.required', ['attribute' => 'Battle']),
            'battle_room_code_id.required' => __('validation.required', ['attribute' => 'Battle Room']),
            'status.required' => __('validation.required', ['attribute' => 'Status']),
            'screenshot.required' => __('validation.required', ['attribute' => 'Screenshot']),
            'admin_status.required' => __('validation.required', ['attribute' => 'Admin Status']),
        ];
    }
}