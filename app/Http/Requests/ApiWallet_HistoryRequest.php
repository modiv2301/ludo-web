<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ApiWallet_HistoryRequest extends FormRequest
{
    protected $stopOnFirstFailure = true;
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required'
        ];
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'user_id.required' => __('validation.required', ['attribute' => 'User Id']),
            
        ];
    }
}