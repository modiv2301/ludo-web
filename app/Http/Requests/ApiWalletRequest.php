<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ApiWalletRequest extends FormRequest
{
    protected $stopOnFirstFailure = true;
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'wallet_amount' => 'required',
            'user_id' => 'required',
            'status' => 'required',
        ];
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'wallet_amount.required' => __('validation.required', ['attribute' => 'Amount']),

            'user_id.required' => __('validation.required', ['attribute' => 'User']),
            'status.required' => __('validation.required', ['attribute' => 'status']),
        ];
    }
}