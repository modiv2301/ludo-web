<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ApiGameHistoryRequest extends FormRequest
{
    protected $stopOnFirstFailure = true;
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'room_code_id' => 'required',
            'user_id' => 'required',
            'game_id' => 'required',
            'wallet_id' => 'required',
        ];
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'room_code_id.required' => __('validation.required', ['attribute' => 'Room Code']),

            'user_id.required' => __('validation.required', ['attribute' => 'User']),
            'game_id.required' => __('validation.required', ['attribute' => 'Game']),
            'wallet_id.required' => __('validation.required', ['attribute' => 'Wallet']),
        ];
    }
}