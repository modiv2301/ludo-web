<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class GameRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (!request()->is('admin/games/create')) {
            return [
                'game_name' => 'required',

                'banner_image' => 'required',
                'no_player' => 'required', //in rs
                'no_battle' => 'required',  //in rs
                'entry_amount' => 'required',
                'win_amount' => 'required',
                'trending_game' => 'required',
                // 'status' => 'required', //1:Open 2:Running
            ];
        } else {
            return [
                'game_name' => 'required',

                'banner_image' => 'required',
                'no_player' => 'required', //in rs
                'no_battle' => 'required',  //in rs
                'entry_amount' => 'required',
                'win_amount' => 'required',
                'trending_game' => 'required',
                // 'status' => 'required', //1:Open 2:Running
            ];
        }
    }

    public function messages()
    {
        return [
            'game_name.required' => __('validation.required', ['attribute' => 'Game Name']),

            'banner_image.required' => __('validation.required', ['attribute' => 'Banner Image']),
            'no_player.required' => __('validation.required', ['attribute' => 'No Player']),
            'no_battle.required' => __('validation.required', ['attribute' => 'No Battle']),
            'entry_amount.required' => __('validation.required', ['attribute' => 'Entry Amount']),
            'winning_amount.required' => __('validation.required', ['attribute' => 'Winning Amount']),
        ];
    }
}
