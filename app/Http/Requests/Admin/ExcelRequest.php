<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ExcelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (!request()->is('admin/excels/create')) {
            return [
                'battle_id' => 'required',

                'excel_file' => 'required',
                'room_code' => 'required', //in rs
               
                // 'use' => 'required',
                // 'status' => 'required',
                // 'status' => 'required', //1:Open 2:Running
            ];
        } else {
            return [
                'battle_id' => 'required',

                'excel_file' => 'required',
                'room_code' => 'required', //in rs
                // 'use' => 'required',  //in rs
                // 'status' => 'required',
                
                // 'status' => 'required', //1:Open 2:Running
            ];
        }
    }

    public function messages()
    {
        return [
            'battle_id.required' => __('validation.required', ['attribute' => 'Battle Name']),

            'excel_file.required' => __('validation.required', ['attribute' => 'Excel']),
            'room_code.required' => __('validation.required', ['attribute' => 'Room']),
           
            
        ];
    }
}
