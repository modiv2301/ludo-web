<?php

namespace App\Http\Controllers\Api\V1\Customer;

use App\Http\Controllers\Controller;
use App\Http\Requests\ApiLoginRequest;
use App\Http\Requests\ApiWallet_HistoryRequest;
use App\Http\Requests\ApiRegisterRequest;
use App\Http\Requests\ApiGameHistoryRequest;
use App\Http\Requests\ApiWalletRequest;
use App\Http\Requests\ApiFindRoomCodeRequest;
use App\Http\Requests\ApiGameResultRequest;
use App\Http\Requests\MobileWithOtpRequest;
use App\Services\Api\AuthService;
use App\Services\HelperService;
use App\Services\UserService;

use App\Services\WalletService;
use Illuminate\Http\Request;

class CAuthController extends Controller
{

    protected $helperService, $userService, $apiAuthService,$walletService;

    public function __construct()
    {
        $this->helperService = new HelperService();
        $this->userService = new UserService();
        $this->walletService = new WalletService();
        $this->apiAuthService = new AuthService();
    }

    /**
     * Authenticate user Check.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(ApiLoginRequest $request)
    {
        return $this->apiAuthService->login($request);
    }

    /**
     * Register user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function new_register(ApiRegisterRequest $request)
    // public function register(ApiRegisterRequest $request)
    {
        
        $request->merge(['role' => 'Customer']);
        return $this->apiAuthService->new_register($request);
    }


    public function wallet(ApiWalletRequest $request)
    {
        return $this->apiAuthService->wallet($request);
    }

    public function wallet_history(ApiWallet_HistoryRequest $request)
    {
          
       
        // $request->merge(['role' => 'Customer']);
        return $this->apiAuthService->wallet_history($request);
    }





    public function allgame()
    {
   
        return $this->apiAuthService->allgame();
    }

    public function allbattle()
    {
   
        return $this->apiAuthService->allbattle();
    }

    public function game_result(ApiGameResultRequest $request)
    {
      
        return $this->apiAuthService->game_result($request);
    }

    

    public function lossgame()
    {
      
        return $this->apiAuthService->lossgame();
    }


    public function game_history(ApiGameHistoryRequest $request)
    {
        
        
        return $this->apiAuthService->game_history($request);
    }

    public function find_room(ApiFindRoomCodeRequest $request)
    {
          
        
        return $this->apiAuthService->find_room($request);
    }

    public function user_data(Request $request)
    {
        return $this->apiAuthService->user_data($request);
    }

    public function battle_use_room(ApiFindRoomCodeRequest $request)
    {  
        return $this->apiAuthService->battle_use_room($request);
    }

    /**
     * Logout user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        return $this->apiAuthService->logout($request);
    }
}