<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ExcelController;
use App\Http\Requests\Admin\GameRequest;
use App\Models\Game;
use App\Services\GameService;
use App\Services\CustomerService;
use App\Services\ManagerLanguageService;
use App\Services\UserService;
use App\Services\UtilityService;
use Illuminate\Http\Request;

class GameController extends Controller
{
    protected $mls, $change_password, $assign_role, $uploads_image_directory;
    protected $index_view, $create_view, $edit_view,$excel_view, $detail_view, $tabe_view, $product_history_view;
    protected $index_route_name, $create_route_name, $detail_route_name, $edit_route_name,$excel_route_name;
    protected $gameService, $utilityService, $customerService;

    public function __construct()
    {
        //Permissions
        // $this->middleware('permission:battle-list|battle-create|battle-edit|battle-delete', ['only' => ['index', 'store']]);
        // $this->middleware('permission:battle-create', ['only' => ['create', 'store']]);
        // $this->middleware('permission:battle-edit', ['only' => ['edit', 'update', 'status']]);
        // $this->middleware('permission:battle-delete', ['only' => ['destroy']]);

        //Data
        $this->uploads_image_directory = 'files/game';
        //route
        $this->index_route_name = 'admin.game.index';
        $this->create_route_name = 'admin.game.create';
        
        $this->detail_route_name = 'admin.game.show';
        $this->edit_route_name = 'admin.game.edit';
        // $this->excel_form_route_name = 'admin.game.excel_form';

        //view files
        $this->index_view = 'admin.game.index';
        $this->create_view = 'admin.game.create';
        
        $this->detail_view = 'admin.game.details';
        $this->tabe_view = 'admin.game.profile';
        $this->edit_view = 'admin.game.edit';
        // $this->excel_form_view = 'admin.game.excel_form';
        
        $this->product_history_view = 'admin.game.product_history';
        $this->change_password = 'admin.admin_profile.change_password';

        //service files
        $this->gameService = new GameService();
        $this->customerService = new CustomerService();
        $this->utilityService = new UtilityService();

        //mls is used for manage language content based on keys in messages.php
        $this->mls = new ManagerLanguageService('messages');
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $items = $this->gameService->datatable();
            // print_r($items);
            // die;
            // $items = $this->battleService->search($request, $items);
            return datatables()->eloquent($items)->toJson();
        } else {
            return view($this->index_view);
        }
    }

    public function create()
    {
        return view($this->create_view);
    }
    
    
    public function excel()
    {
        return view($this->create_view);
    }
    
    // public function excel()
    // {
    //     echo"Hello";
    //     die;
    //     return view($this->excel_view);
    // }

    public function store(GameRequest $request)
    {
    
          
        $input = $request->except(['_token', 'proengsoft_jsvalidation']);   
        $logo=$request->file('banner_image');

        $filename    = time().$logo->getClientOriginalName();
        $destinationPath = public_path('/files/game/');

        $logo->move($destinationPath, $filename);
        $input['banner_image']=$filename;
        $game = $this->gameService->create($input);

        return redirect()->route($this->index_route_name)
            ->with('success', $this->mls->messageLanguage('created', 'game', 1));
            

    }

    // public function excel_store(GameRequest $request)
    // {
    
    //     $input = $request->except(['_token', 'proengsoft_jsvalidation']);
    //     // print_r($input);
    //     // die;
    //     $logo=$request->file('excel_file');

    //     $filename    = time().$logo->getClientOriginalName();
    //     $destinationPath = public_path('/files/game/');

    //     $logo->move($destinationPath, $filename);
    //     $input['excel_file']=$filename;
    //     $game = $this->gameService->create($input);

    //     return redirect()->route($this->index_route_name)
    //         ->with('success', $this->mls->messageLanguage('created', 'game', 1));

    // }


    public function show(Game $game)
    {
        return view($this->detail_view, compact('game'));
    }

    public function edit(Game $game)
    {
        return view($this->edit_view, compact('game'));
    }
    // public function excel_form(Game $game)
    // {
    //     return view($this->excel_view, compact('game'));
    // }

    public function update(GameRequest $request, Game $game)
    {
        $input = $request->except(['_method', '_token', 'proengsoft_jsvalidation']);
        $this->gameService->update($input, $game);
        return redirect()->route($this->index_route_name)
            ->with('success', $this->mls->messageLanguage('updated', 'game', 1));
    }

    public function destroy(Game $game)
    {
        $result = $game->delete();
        if ($result) {
            return response()->json([
                'status' => 1,
                'title' => $this->mls->onlyNameLanguage('deleted_title'),
                'message' => $this->mls->onlyNameLanguage('game'),
                'status_name' => 'success'
            ]);
        } else {
            return response()->json([
                'status' => 0,
                'title' => $this->mls->onlyNameLanguage('deleted_title'),
                'message' => $this->mls->onlyNameLanguage('game'),
                'status_name' => 'error'
            ]);
        }
    }

    public function status($id, $status)
    {
        $status = ($status == 1) ? 0 : 1;
        $result =  $this->gameService->updateById(['is_active' => $status], $id);
        if ($result) {
            return response()->json([
                'status' => 1,
                'message' => $this->mls->messageLanguage('updated', 'status', 1),
                'status_name' => 'success'
            ]);
        } else {
            return response()->json([
                'status' => 0,
                'message' => $this->mls->messageLanguage('not_updated', 'status', 1),
                'status_name' => 'error'
            ]);
        }
    }
}
