<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ExcelController;
use App\Http\Requests\Admin\GameRequest;
use App\Http\Requests\Admin\ApiGame_ResultRequest;
use App\Models\Wallet;
use App\Services\WalletService;
use App\Services\CustomerService;
use App\Services\ManagerLanguageService;
use App\Services\UserService;
use App\Services\UtilityService;
use Illuminate\Http\Request;

class WalletHistoryController extends Controller
{
    protected $mls, $change_password, $assign_role, $uploads_image_directory;
    protected $index_view, $create_view, $edit_view,$excel_view, $detail_view, $tabe_view, $product_history_view;
    protected $index_route_name, $create_route_name, $detail_route_name, $edit_route_name,$excel_route_name;
    protected $resultService, $utilityService, $customerService;

    public function __construct()
    {
        //Permissions
        // $this->middleware('permission:battle-list|battle-create|battle-edit|battle-delete', ['only' => ['index', 'store']]);
        // $this->middleware('permission:battle-create', ['only' => ['create', 'store']]);
        // $this->middleware('permission:battle-edit', ['only' => ['edit', 'update', 'status']]);
        // $this->middleware('permission:battle-delete', ['only' => ['destroy']]);

        //Data
        $this->uploads_image_directory = 'files/game';
        //route
        $this->index_route_name = 'admin.wallethistory.index';
        
        $this->create_route_name = 'admin.wallethistory.create';
        
        $this->detail_route_name = 'admin.wallethistory.show';
        $this->edit_route_name = 'admin.wallethistory.edit';
        // $this->excel_form_route_name = 'admin.game.excel_form';

        //view files
        $this->index_view = 'admin.wallethistory.index';
        $this->create_view = 'admin.wallethistory.create';
        
        $this->detail_view = 'admin.wallethistory.details';
        $this->tabe_view = 'admin.wallethistory.profile';
        $this->edit_view = 'admin.wallethistory.edit';
        // $this->excel_form_view = 'admin.game.excel_form';
        
        $this->product_history_view = 'admin.wallethistory.product_history';
        $this->change_password = 'admin.admin_profile.change_password';

        //service files
        $this->walletService = new WalletService();
        // $this->customerService = new CustomerService();
        // $this->utilityService = new UtilityService();

        //mls is used for manage language content based on keys in messages.php
        $this->mls = new ManagerLanguageService('messages');
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $items = $this->walletService->datatable();
            // print_r($items);
            // die;
            // $items = $this->battleService->search($request, $items);
            return datatables()->eloquent($items)->toJson();
        } else {
            return view($this->index_view);
        }
    }

    public function create()
    {
        return view($this->create_view);
    }
    



    public function show(Wallet $game)
    {
            
        return view($this->detail_view, compact('game_result'));
    }

    
    // public function excel_form(Game $game)
    // {
    //     return view($this->excel_view, compact('game'));
    // }

   

    

    public function status($id, $status)
    {
        $status = ($status == 1) ? 0 : 1;
        $result =  $this->resultService->updateById(['is_active' => $status], $id);
        if ($result) {
            return response()->json([
                'status' => 1,
                'message' => $this->mls->messageLanguage('updated', 'status', 1),
                'status_name' => 'success'
            ]);
        } else {
            return response()->json([
                'status' => 0,
                'message' => $this->mls->messageLanguage('not_updated', 'status', 1),
                'status_name' => 'error'
            ]);
        }
    }
}
