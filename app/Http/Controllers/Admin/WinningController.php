<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\WinningRequest;
use App\Models\Winning;
use App\Services\CustomerService;
use App\Services\ManagerLanguageService;
use App\Services\UtilityService;
use App\Services\WinningService;
use Illuminate\Http\Request;

class WinningController extends Controller
{
    protected $mls, $change_password, $assign_role, $uploads_image_directory;
    protected $index_view, $create_view, $edit_view, $detail_view, $tabe_view, $product_history_view;
    protected $index_route_name, $create_route_name, $detail_route_name, $edit_route_name;
    protected $winningService, $utilityService, $customerService;

    public function __construct()
    {
        //Permissions
        // $this->middleware('permission:winning-list|winning-create|winning-edit|winning-delete', ['only' => ['index', 'store']]);
        // $this->middleware('permission:winning-create', ['only' => ['create', 'store']]);
        // $this->middleware('permission:winning-edit', ['only' => ['edit', 'update', 'status']]);
        // $this->middleware('permission:winning-delete', ['only' => ['destroy']]);
        
        //Data

        $this->uploads_image_directory = 'files/winnings';
        //route
        $this->index_route_name = 'admin.winnings.index';
        $this->create_route_name = 'admin.winnings.create';
        $this->detail_route_name = 'admin.winnings.show';
        $this->edit_route_name = 'admin.winnings.edit';

        //view files
        $this->index_view = 'admin.winning.index';
        $this->create_view = 'admin.winning.create';
        $this->detail_view = 'admin.winning.details';
        $this->tabe_view = 'admin.winning.profile';
        $this->edit_view = 'admin.winning.edit';
        $this->product_history_view = 'admin.winning.product_history';
        $this->change_password = 'admin.admin_profile.change_password';

        //service files
        $this->winningService = new WinningService();
        $this->customerService = new CustomerService();
        $this->utilityService = new UtilityService();

        //mls is used for manage language content based on keys in messages.php
        $this->mls = new ManagerLanguageService('messages');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $items = $this->winningService->datatable();
            // $items = $this->winningService->search($request, $items);
            return datatables()->eloquent($items)->toJson();
        } else {
            return view($this->index_view);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $players = $this->customerService->getCustomerIdName();
        return view($this->create_view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  WinningRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(WinningRequest $request)
    {
        $input = $request->except(['_token', 'proengsoft_jsvalidation']);
        $winning = $this->winningService->create($input);
        return redirect()->route($this->index_route_name)
            ->with('success', $this->mls->messageLanguage('created', 'winning', 1));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Winning  $winning
     * @return \Illuminate\Http\Response
     */
    public function show(Winning $winning)
    {
        return view($this->detail_view, compact('winning'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Winning  $winning
     * @return \Illuminate\Http\Response
     */
    public function edit(Winning $winning)
    {
        return view($this->edit_view, compact('winning'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Winning  $winning
     * @return \Illuminate\Http\Response
     */
    public function update(WinningRequest $request, Winning $winning)
    {
        $input = $request->except(['_method', '_token', 'proengsoft_jsvalidation']);
        $this->winningService->update($input, $winning);
        return redirect()->route($this->index_route_name)
            ->with('success', $this->mls->messageLanguage('updated', 'winning', 1));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Winning  $winning
     * @return \Illuminate\Http\Response
     */
    public function destroy(Winning $winning)
    {
        $result = $winning->delete();
        if ($result) {
            return response()->json([
                'status' => 1,
                'title' => $this->mls->onlyNameLanguage('deleted_title'),
                'message' => $this->mls->onlyNameLanguage('winning'),
                'status_name' => 'success'
            ]);
        } else {
            return response()->json([
                'status' => 0,
                'title' => $this->mls->onlyNameLanguage('deleted_title'),
                'message' => $this->mls->onlyNameLanguage('winning'),
                'status_name' => 'error'
            ]);
        }
    }

    public function status($id, $status)
    {
        $status = ($status == 1) ? 0 : 1;
        $result =  $this->winningService->updateById(['is_active' => $status], $id);
        if ($result) {
            return response()->json([
                'status' => 1,
                'message' => $this->mls->messageLanguage('updated', 'status', 1),
                'status_name' => 'success'
            ]);
        } else {
            return response()->json([
                'status' => 0,
                'message' => $this->mls->messageLanguage('not_updated', 'status', 1),
                'status_name' => 'error'
            ]);
        }
    }
}
