<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ExcelRequest;
use App\Models\Game;
use App\Services\ExcelService;
use App\Services\CustomerService;
use App\Services\ManagerLanguageService;
use App\Services\UserService;
use App\Services\UtilityService;
use Illuminate\Http\Request;

class ExcelController extends Controller
{
    protected $mls, $change_password, $assign_role, $uploads_image_directory;
    protected $index_view, $create_view, $edit_view,$excel_view, $detail_view, $tabe_view, $product_history_view;
    protected $index_route_name, $create_route_name, $detail_route_name, $edit_route_name,$excel_route_name;
    protected $excelService, $utilityService, $customerService;

    public function __construct()
    {
        //Permissions
        // $this->middleware('permission:battle-list|battle-create|battle-edit|battle-delete', ['only' => ['index', 'store']]);
        // $this->middleware('permission:battle-create', ['only' => ['create', 'store']]);
        // $this->middleware('permission:battle-edit', ['only' => ['edit', 'update', 'status']]);
        // $this->middleware('permission:battle-delete', ['only' => ['destroy']]);

        //Data
        $this->uploads_image_directory = 'files/game';
        //route
        $this->index_route_name = 'admin.excel.index';
        $this->create_route_name = 'admin.excel.create';
        $this->excel_route_name = 'admin.excel.excel';
        // $this->detail_route_name = 'admin.game.show';
        $this->edit_route_name = 'admin.excel.edit';
        // $this->excel_form_route_name = 'admin.game.excel_form';

        //view files
        // $this->index_view = 'admin.excel.index';
        $this->create_view = 'admin.excel.create';
        // $this->excel_view = 'admin.excel.excel';
        $this->detail_view = 'admin.excel.details';
        $this->tabe_view = 'admin.excel.profile';
        $this->edit_view = 'admin.excel.edit';
        // $this->excel_form_view = 'admin.game.excel_form';
        
        $this->product_history_view = 'admin.excel.product_history';
        $this->change_password = 'admin.admin_profile.change_password';

        //service files
        $this->excelService = new ExcelService();
        $this->customerService = new CustomerService();
        $this->utilityService = new UtilityService();

        //mls is used for manage language content based on keys in messages.php
        $this->mls = new ManagerLanguageService('messages');
    }

   

    public function create()
    {
        // echo"Hello";
        // die;
        return view($this->create_view);
    }
        public function store(ExcelRequest $request)
    {
       
        $input = $request->except(['_token', 'proengsoft_jsvalidation']);
        // print_r($input);
        // die;
        $logo=$request->file('excel_file');

        $filename    = time().$logo->getClientOriginalName();
        $destinationPath = public_path('/files/game/');

        $logo->move($destinationPath, $filename);
        $input['excel_file']=$filename;
        $excel = $this->excelService->create($input);

        return redirect()->route($this->create_route_name)
            ->with('success', $this->mls->messageLanguage('created', 'excel', 1));

    }
}
