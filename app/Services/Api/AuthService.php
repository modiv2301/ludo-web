<?php

namespace App\Services\Api;

use App\Http\Requests\ApiLoginRequest;
use App\Http\Requests\ApiRegisterRequest;
use App\Http\Requests\ApiWallet_HistoryRequest;
use App\Http\Requests\ApiWalletRequest;

use App\Http\Requests\ApiGameResultRequest;
use App\Http\Requests\ApiGameHistoryRequest;
use App\Http\Requests\ApiFindRoomCodeRequest;
use App\Models\MasterOtp;
use App\Models\User;
use App\Models\Wallet;
use App\Models\Withdraw;
use App\Models\BattleRoomCode;
use App\Models\Winning;
use App\Models\GameResult;
use App\Models\GameHistory;
use App\Notifications\NewUserNotify;
use App\Services\HelperService;
use App\Services\UserService;
use App\Services\WalletService;
use App\Services\GameResultService;
use App\Services\GameHistoryService;
use App\Services\WithdrawService;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\DB;

class AuthService
{
    /**
     * Authenticate user Check and login.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public static function login(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return response()->json(
                [
                    'status' => false,
                    'message' => "You don't have an account with us, Please create your account with us and then login.",
                    'type' => 'unauthorized',
                ],
                200
            );
        }

        $credentials = $request->only(['email', 'password']);
        $credentials['is_active'] = 1;
        $token = auth('api')->attempt($credentials, ['exp' => Carbon::now()->addDays(60)->timestamp]);

        if (!$token) {
            if ($user->is_active == 0) {
                return response()->json(
                    [
                        'status' => false,
                        'message' => 'Your account has been deactivated by admin. Please contact to Support Team.',
                        'type' => 'unauthorized',
                    ],
                    200
                );
            } else {
                return response()->json(
                    [
                        'status' => false,
                        'message' => 'Oops!, You have provide incorrect credentials.',
                        'type' => 'unauthorized',
                    ],
                    200
                );
            }
        }
        $user = JWTAuth::setToken($token)->toUser();

        if ($user->is_active == 1) {
            // UserService::updateLastLogin($user->id, $request);
            return response()->json(
                [
                    'status' => true,
                    'message' => 'login successfully',
                    'token' => $token,
                    'data' => $user
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Deactive user',
                    'type' => 'unauthorized',
                ],
                200
            );
        }
    }

    /**
     * Register user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public static function new_register(ApiRegisterRequest $request)
    // public static function new_register(ApiRegisterRequest $request)
    {
        $is_register = false;
        $user = User::where('email', $request->email)->first();
        if ($user) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'You have an account, Please login with same credentials.',
                    'type' => 'unauthorized',
                ],
                200
            );
        } else {
            $is_register = true;
            $input = array_merge(
                $request->except(['_token']),
                [
                    'name' => $request->name,
                    'email' => $request->email,
                    'is_active' => 1,
                    'password' => Hash::make($request->password),
                ]
            );

            $user = UserService::create($input);
            $user->assignRole($request->role);

            // $admin_user = UserService::getAdminUser();
            // try {
            //     $admin_user->notify(new NewUserNotify($user));
            // } catch (Exception $e) {
            //     Log::error("AuthController -  registerWithOtp - " . $e->getMessage());
            // }

            $token = auth('api')->login($user, ['exp' => Carbon::now()->addDays(120)->timestamp]);

            if (!$token) {
                return response()->json(
                    [
                        'status' => false,
                        'message' => 'unauthorized',
                        'type' => 'unauthorized',
                    ],
                    200
                );
            }

            $user = JWTAuth::setToken($token)->toUser();
            // UserService::updateLastLogin($user->id, $request);

            // For User firebase token Subscribe To Topic
            // HelperService::firebaseTokenSubscribeToTopic('Customer', $user->device_id);

            if ($user->is_active == 1) {
                return response()->json(
                    [
                        'status' => true,
                        'message' => 'Logged in successfully',
                        'is_register' => $is_register,
                        'token' => $token,
                        'data' => $user
                    ],
                    200
                );
            } else {
                return response()->json(
                    [
                        'status' => false,
                        'message' => 'Deactive user',
                        'type' => 'unauthorized',
                    ],
                    200
                );
            }
        }
    }

    public static function allgame(){
        $games = DB::table('games')->get();
        if ($games) {
            
            foreach ($games as $game) {
                
                $games_image=public_path('/files/game/').$game->banner_image;
            
                    $game->banner_image = $games_image;
                }
            
            return response()->json(
                [
                    'status' => true,
                    'message' => 'Data Find successfully',
                    'data' => $games
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Data not Found',
                    'data' =>[],
                ],
                200
            );
        }
    }


    public static function allbattle(){
        $battle = DB::table('battle_room_code')->get();
        if ($battle) {
            return response()->json(
                [
                    'status' => true,
                    'message' => 'Data Find successfully',
                    'data' => $battle
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Data not Found',
                    'data' =>[],
                ],
                200
            );
        }
    }



    public static function wallet(ApiWalletRequest $request){
     
        if($request->status==0){
        $ch = curl_init();
        $fields = array();
        $fields["amount"] = $request->wallet_amount;
        $fields["currency"] = "INR";
        curl_setopt($ch, CURLOPT_URL, 'https://api.razorpay.com/v1/payments/'.$request->transaction_id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_USERPWD, "rzp_test_GYl71wgx7cqEWs:4hmehHJXTzfdDn1lO4zxUrvl");
        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $data = curl_exec($ch);
    
        if (empty($data) OR (curl_getinfo($ch, CURLINFO_HTTP_CODE != 200))) {
           $data = FALSE;
        } else {
            $data=json_decode($data, TRUE);
            // return $data;
        }
        if(!empty($data['error'])|| !$data||$data['amount']!=$request->wallet_amount){
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Invalid Data ',
                    'data' => $data 
                ],
                200
            );
        }
        
        curl_close($ch);
    
        $input = 
            [
                'wallet_amount' => $request->wallet_amount,
                'user_id' => $request->user_id,
                'transaction_id' => $request->transaction_id,
                'status' => $request->status,

                
            ];
    }else{
        
        $input = 
            [
                'wallet_amount' => $request->wallet_amount,
                'user_id' => $request->user_id,
                'transaction_id' => '',
                'status' => $request->status,
            ];
    }
        
        $user = WalletService::create($input);
        
        if($request->status==0){
            User::where('id',$request->user_id)->increment('wallet_amount',$request->wallet_amount);
        }
        else{

            User::where('id',$request->user_id)->decrement('wallet_amount',$request->wallet_amount);
        }
        if ($user) {
            return response()->json(
                [
                    'status' => true,
                    'message' => 'Data Insert successfully',
                    'data' => $user 
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Data not Found',
                    'data' =>[],
                ],
                200
            );
        }
    }

    public static function wallet_history(ApiWallet_HistoryRequest $request){
        $user = Wallet::where('user_id', $request->user_id)->get();
        if ($user) {
            return response()->json(
                [
                    'status' => true,
                    'message' => 'Data Find successfully',
                    'data' => $user
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Data not Found',
                    'data' =>[],
                ],
                200
            );
        }
    }

    public static function game_result(ApiGameResultRequest $request){
        
        $input = 
        [
            'user_id' => $request->user_id,
            'battle_id' => $request->battle_id,
            'battle_room_code_id' => $request->battle_room_code_id,
            'status' => $request->status,
            'screenshot' => $request->screenshot,
            'admin_status' => $request->admin_status,
        ];
        $logo=$request->file('screenshot');

        $filename    = time().$logo->getClientOriginalName();
        $destinationPath = public_path('/screenshots/game/');

        $logo->move($destinationPath, $filename);
        $input['screenshot']=$filename;
        $game_result = GameResultService::create($input);
      
        if ($game_result) {
            return response()->json(
                [
                    'status' => true,
                    'message' => 'Data Insert successfully',
                    'data' => $game_result 
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Data not Found',
                    'data' =>[],
                ],
                200
            );
        }
    }

    public static function lossgame(){
        $loss = GameResult::where('status','2')->get();
        if (!empty($loss)) {
            return response()->json(
                [
                    'status' => true,
                    'message' => 'Data Find successfully',
                    'data' => $loss
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Data not Found',
                    'data' =>[],
                ],
                200
            );
        }
    }
    
    public static function game_history(ApiGameHistoryRequest $request){
        
        $input = 
        [
            'room_code_id' => $request->room_code_id,
            'user_id' => $request->user_id,
            'game_id' => $request->game_id,
            'wallet_id' => $request->wallet_id,
        
        ];
       
        $game_history = GameHistoryService::create($input);
      
        if ($game_history) {
            return response()->json(
                [
                    'status' => true,
                    'message' => 'Data Insert successfully',
                    'data' => $game_history 
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Data not Found',
                    'data' =>[],
                ],
                200
            );
        }
    }

    public static function find_room(ApiFindRoomCodeRequest $request){
       
        $room = BattleRoomCode::where('battle_id', $request->battle_id)->get();
        if ($room) {
            return response()->json(
                [
                    'status' => true,
                    'message' => 'Data Find successfully',
                    'data' => $room
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Data not Found',
                    'data' =>[],
                ],
                200
            );
        }
    }

    public static function battle_use_room(Request $request){
       
        $room = BattleRoomCode::where('battle_id', $request->battle_id)->
        where('use_room', '!=', $request->use_room)->limit(1)->
    get();
    // echo count($room);die;
        if (count($room)>0) {
            return response()->json(
                [
                    'status' => true,
                    'message' => 'Data Find successfully',
                    'data' => $room
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Data not Found',
                    'data' =>[],
                ],
                200
            );
        }
    }

    public static function user_data(Request $request){
        $user = User::where('id', $request->user_id)->get();
        if ($user) {
            return response()->json(
                [
                    'status' => true,
                    'message' => 'Data Find successfully',
                    'data' => $user
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Data not Found',
                    'data' =>[],
                ],
                200
            );
        }
    }

    public static function withdraw(Request $request){
        
        $input = 
        [
            
            'user_id' => $request->user_id,
            'withdraw_amount' => $request->withdraw_amount,
            'status' => $request->status,
        
        ];
       
        $game_history = WithdrawService::create($input);
      
        if ($game_history) {
            return response()->json(
                [
                    'status' => true,
                    'message' => 'Data Insert successfully',
                    'data' => $game_history 
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'Data not Found',
                    'data' =>[],
                ],
                200
            );
        }
    }



    /**
     * Logout user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public static function logout(Request $request)
    {
        self::getAuthUser();
        JWTAuth::invalidate(JWTAuth::getToken());
        return response()->json([
            'status' => true,
            'message' => 'Successfully logged out'
        ]);
    }

    public static function getAuthUser()
    {
        return JWTAuth::parseToken()->authenticate();
    }
}
