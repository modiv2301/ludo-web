<?php

namespace App\Services\Api;

use App\Models\MasterOtp;
use App\Models\User;
use App\Notifications\NewUserNotify;
use App\Services\HelperService;
use App\Services\UserService;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthService
{
    /**
     * Authenticate user Check and sendOtp.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public static function sendOtp(Request $request)
    {
        $otp = HelperService::createOtp();
        $input = [
            'mobile_no' => $request->mobile_no,
            'otp' => $otp,
            'role' => 3,
        ];
        MasterOtp::create($input);
        return response()->json(
            [
                'status' => true,
                'message' => 'Valid mobile number and send message',
                'data' => [
                    'otp' => $otp
                ]
            ],
            200
        );
    }

    /**
     * Authenticate login user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public static function verifyOtp(Request $request)
    {
        $data = MasterOtp::where('mobile_no', $request->mobile_no)
            ->where('otp', $request->otp)
            ->orderBy('created_at', 'desc')
            // ->whereBetween('created_at', [now()->subMinutes(10), now()])
            ->first();
        // dd($data);
        if ($data) {
            $is_register = false;
            $user = User::where('mobile_no', $request->mobile_no)->first();
            if (!$user) {
                $is_register = true;

                $input = array_merge(
                    $request->except(['_token', 'otp']),
                    [
                        'name' => 'Guest User',
                        'is_active' => 1,
                        'password' => Hash::make('User@123'),
                    ]
                );

                $user = UserService::create($input);
                $user->assignRole($request->role);
                $admin_user = UserService::getAdminUser();
                try {
                    $admin_user->notify(new NewUserNotify($user));
                } catch (Exception $e) {
                    Log::error("AuthController -  registerWithOtp - " . $e->getMessage());
                }
            }
            if ($user->hasRole($request->role)) {
                $token = auth('api')->login($user, ['exp' => Carbon::now()->addDays(120)->timestamp]);
                $credentials = $user->only(['mobile_no', 'password']);
                $credentials['is_active'] = 1;

                if (!$token) {
                    return response()->json(
                        [
                            'status' => false,
                            'message' => 'unauthorized',
                            'type' => 'unauthorized',
                        ],
                        200
                    );
                }
                $user = JWTAuth::setToken($token)->toUser();
                // UserService::updateLastLogin($user->id, $request);

                // For User firebase token Subscribe To Topic
                HelperService::firebaseTokenSubscribeToTopic('Customer', $user->device_id);

                if ($user->is_active == 1) {
                    $data->delete(); //Delete Master Otp
                    return response()->json(
                        [
                            'status' => true,
                            'message' => 'Logged in successfully',
                            'is_register' => $is_register,
                            'token' => $token,
                            'data' => $user
                        ],
                        200
                    );
                } else {
                    return response()->json(
                        [
                            'status' => false,
                            'message' => 'Deactive user',
                            'type' => 'unauthorized',
                        ],
                        200
                    );
                }
            } else {
                return response()->json(
                    [
                        'status' => false,
                        'message' => 'Unauthorized User',
                        'type' => 'unauthorized',
                    ],
                    200
                );
            }
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Wrong OTP',
                'errors' => ['otp' => ['Wrong OTP']]
            ], 200);
        }
    }

    /**
     * Logout user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public static function logout(Request $request)
    {
        self::getAuthUser();
        JWTAuth::invalidate(JWTAuth::getToken());
        return response()->json([
            'status' => true,
            'message' => 'Successfully logged out'
        ]);
    }

    public static function getAuthUser()
    {
        return JWTAuth::parseToken()->authenticate();
    }
}