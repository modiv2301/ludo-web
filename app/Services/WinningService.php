<?php

namespace App\Services;

use App\Models\Winning;

class WinningService
{
    /**
     * Update the specified resource in storage.
     *
     * @param  array $data
     * @return Winning
     */
    public static function create(array $data)
    {
        $data = Winning::create($data);
        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Array $data - Updated Data
     * @param  Winning $winning
     * @return Winning
     */
    public static function update(array $data, Winning $winning)
    {
        $data = $winning->update($data);
        return $data;
    }

    /**
     * UpdateById the specified resource in storage.
     *
     * @param  Array $data - Updated Data
     * @param  $id
     * @return Winning
     */
    public static function updateById(array $data, $id)
    {
        $data = Winning::whereId($id)->update($data);
        return $data;
    }

    /**
     * Get Data By Id from storage.
     *
     * @param  Int $id
     * @return Winning
     */
    public static function getById($id)
    {
        $data = Winning::find($id);
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  App\Models\Winning
     * @return bool
     */
    public static function delete(Winning $winning)
    {
        $data = $winning->delete();
        return $data;
    }

    /**
     * RemoveById the specified resource from storage.
     *
     * @param  $id
     * @return bool
     */
    public static function deleteById($id)
    {
        $data = Winning::whereId($id)->delete();
        return $data;
    }

    /**
     * update data in storage.
     *
     * @param  Array $data - Updated Data
     * @param  Int $id - Winning Id
     * @return bool
     */
    public static function status(array $data, $id)
    {
        $data = Winning::where('id', $id)->update($data);
        return $data;
    }

    /**
     * Get data for datatable from storage.
     *
     * @return Winning with states, countries
     */
    public static function datatable()
    {
        $data = Winning::orderBy('created_at', 'desc');
        return $data;
    }
}
