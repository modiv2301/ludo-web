<?php

namespace App\Services;

use App\Models\GameHistory;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class GameHistoryService
{
    /**
     * Create the specified resource.
     *
     * @param Request $request
     * @return User
     */
    public static function create(array $data)
    {
        $data = GameHistory::create($data);
        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return bool
     */
    public static function updateById(array $data, $user_id)
    {
        $data = GameHistory::where('id', $user_id)->update($data);
        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Array $data
     * @param  App\Models\User  $user
     * @return bool
     */
    // public static function update(array $data, User $user)
    // {
    //     $data = $user->update($data);
    //     return $data;
    // }

    /**
     * Get the specified resource in storage.
     *
     * @param int $id
     * @return  App\Models\User  $user
     */
    public static function getById($id)
    {
        $data = GameHistory::with('roles')->find($id);
        return $data;
    }

    /**
     * Get the specified resource in storage.
     *
     * @return  App\Models\User  $data
     */
    public static function getAdminUser()
    {
        $data = GameHistory::find(1);
        return $data;
    }

    /**
     * Get data by $parameters.
     *
     * @param Array $parameters
     * @return Model
     */
    public static function getByParameters($parameters)
    {
        $data = GameHistory::query();
        foreach ($parameters as $parameter) {
            $data = $data->where($parameter['column_name'], $parameter['value']);
        }
        return $data;
    }

    /**
     * Delete data by user.
     *
     * @param User $user
     * @return bool
     */
    public static function delete(GameHistory $user)
    {
        $data = $user->delete();
        return $data;
    }

    /**
     * Fetch records for datatables
     */
    public static function datatable()
    {
        $data = GameHistory::with('roles')->whereHas("roles", function ($q) {
            $q->whereNotIn("name", ['Admin']);
        });
        return $data;
    }

    /**
     * update status.
     *
     * @param Array $data
     * @param int $id
     * @return bool
     */
    public static function status(array $data, $id)
    {
        $data = GameHistory::where('id', $id)->update($data);
        return $data;
    }

    /**
     * update Last Login details.
     *
     * @param int $id
     * @param Request $request = null
     * @return bool
     */
   

    /**
     * Get user with relations
     *
     * @param Int $id
     * @param Array $relations
     * @return \App\Models\User
     */
    


   


    
   

    /**
     * Get data for download Report from storage.
     *
     * @return User with all its Client data
     */
    

    /**
     * Delete the old user image
     */
  
}