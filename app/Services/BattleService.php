<?php

namespace App\Services;

use App\Models\Battle;

class BattleService
{
    /**
     * Update the specified resource in storage.
     *
     * @param  array $data
     * @return Battle
     */
    public static function create(array $data)
    {
        $data = Battle::create($data);
        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Array $data - Updated Data
     * @param  Battle $battle
     * @return Battle
     */
    public static function update(array $data, Battle $battle)
    {
        $data = $battle->update($data);
        return $data;
    }

    /**
     * UpdateById the specified resource in storage.
     *
     * @param  Array $data - Updated Data
     * @param  $id
     * @return Battle
     */
    public static function updateById(array $data, $id)
    {
        $data = Battle::whereId($id)->update($data);
        return $data;
    }

    /**
     * Get Data By Id from storage.
     *
     * @param  Int $id
     * @return Battle
     */
    public static function getById($id)
    {
        $data = Battle::find($id);
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  App\Models\Battle
     * @return bool
     */
    public static function delete(Battle $battle)
    {
        $data = $battle->delete();
        return $data;
    }

    /**
     * RemoveById the specified resource from storage.
     *
     * @param  $id
     * @return bool
     */
    public static function deleteById($id)
    {
        $data = Battle::whereId($id)->delete();
        return $data;
    }

    /**
     * update data in storage.
     *
     * @param  Array $data - Updated Data
     * @param  Int $id - Battle Id
     * @return bool
     */
    public static function status(array $data, $id)
    {
        $data = Battle::where('id', $id)->update($data);
        return $data;
    }

    /**
     * Get data for datatable from storage.
     *
     * @return Battle with states, countries
     */
    public static function datatable()
    {
        $data = Battle::orderBy('created_at', 'desc');
        return $data;
    }
}
