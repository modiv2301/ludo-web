<?php

namespace App\Services;

use App\Models\Withdraw;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class WithdrawService
{
    /**
     * Create the specified resource.
     *
     * @param Request $request
     * @return User
     */
    public static function create(array $data)
    {
        $data = Withdraw::create($data);
        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return bool
     */
    public static function updateById(array $data, $user_id)
    {
        $data = Withdraw::where('id', $user_id)->update($data);
        return $data;
    }

   
    public static function getById($id)
    {
        $data = Withdraw::with('roles')->find($id);
        return $data;
    }

    /**
     * Get the specified resource in storage.
     *
     * @return  App\Models\User  $data
     */
    public static function getAdminUser()
    {
        $data = Withdraw::find(1);
        return $data;
    }

    /**
     * Get data by $parameters.
     *
     * @param Array $parameters
     * @return Model
     */
    public static function getByParameters($parameters)
    {
        $data = Withdraw::query();
        foreach ($parameters as $parameter) {
            $data = $data->where($parameter['column_name'], $parameter['value']);
        }
        return $data;
    }

    /**
     * Delete data by user.
     *
     * @param User $user
     * @return bool
     */
    public static function delete(Withdraw $user)
    {
        $data = $user->delete();
        return $data;
    }

    /**
     * Fetch records for datatables
     */
    public static function datatable()
    {
        $data = Withdraw::orderBy('created_at', 'desc');
      
        return $data;
    }

    /**
     * update status.
     *
     * @param Array $data
     * @param int $id
     * @return bool
     */
    public static function status(array $data, $id)
    {
        $data = Withdraw::where('id', $id)->update($data);
        return $data;
    }

    /**
     * update Last Login details.
     *
     * @param int $id
     * @param Request $request = null
     * @return bool
     */
   

   


    

  

   

  

}