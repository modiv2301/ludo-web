<?php

namespace App\Services;


use App\Models\GameResult;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class GameResultService
{
   
    public static function create(array $data)
    {
        $data = GameResult::create($data);
        return $data;
    }

    
    public static function updateById(array $data, $user_id)
    {
        $data = GameResult::where('id', $user_id)->update($data);
        return $data;
    }

   
    public static function getById($id)
    {
        $data = GameResult::with('roles')->find($id);
        return $data;
    }

    public static function datatable()
    {
        $reasult=GameResult::where('status',0);
        
        return $reasult;
    }

   
  

   
   
    /**
     * Fetch records for datatables
     */
  

   


   
 

   
}