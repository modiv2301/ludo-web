<?php

namespace App\Services;

use App\Models\Game;

class GameService
{
    /**
     * Update the specified resource in storage.
     *
     * @param  array $data
     * @return Game
     */
    public static function create(array $data)
    {
        $data = Game::create($data);
        return $data;
    }
     public static function excel(array $data)
    {
        $data = Game::excel($data);
        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Array $data - Updated Data
     * @param  Game $battle
     * @return Game
     */
    public static function update(array $data, Game $game)
    {
        $data = $game->update($data);
        return $data;
    }

    /**
     * UpdateById the specified resource in storage.
     *
     * @param  Array $data - Updated Data
     * @param  $id
     * @return Game
     */
    public static function updateById(array $data, $id)
    {
        $data = Game::whereId($id)->update($data);
        return $data;
    }

    /**
     * Get Data By Id from storage.
     *
     * @param  Int $id
     * @return Game
     */
    public static function getById($id)
    {
        $data = Game::find($id);
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  App\Models\Battle
     * @return bool
     */
    public static function delete(Game $game)
    {
        $data = $game->delete();
        return $data;
    }

    /**
     * RemoveById the specified resource from storage.
     *
     * @param  $id
     * @return bool
     */
    public static function deleteById($id)
    {
        $data = Game::whereId($id)->delete();
        return $data;
    }

    /**
     * update data in storage.
     *
     * @param  Array $data - Updated Data
     * @param  Int $id - Battle Id
     * @return bool
     */
    public static function status(array $data, $id)
    {
        $data = Game::where('id', $id)->update($data);
        return $data;
    }

    /**
     * Get data for datatable from storage.
     *
     * @return Game with states, countries
     */
    public static function datatable()
    {
        $data = Game::orderBy('created_at', 'desc');
      
        return $data;
    }
}
