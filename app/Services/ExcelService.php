<?php

namespace App\Services;

use App\Models\Excel;

class ExcelService
{
    /**
     * Update the specified resource in storage.
     *
     * @param  array $data
     * @return Game
     */
    public static function create(array $data)
    {
        $data = Excel::create($data);
        return $data;
    }
     public static function excel(array $data)
    {
        $data = Excel::excel($data);
        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Array $data - Updated Data
     * @param  Game $battle
     * @return Game
     */
    public static function update(array $data, Excel $game)
    {
        $data = $game->update($data);
        return $data;
    }

    /**
     * UpdateById the specified resource in storage.
  
     */
    public static function updateById(array $data, $id)
    {
        $data = Excel::whereId($id)->update($data);
        return $data;
    }

    /**
     * Get Data By Id from storage.
     *
     * @param  Int $id
     * @return Game
     */
    public static function getById($id)
    {
        $data = Excel::find($id);
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @p
     */
    public static function delete(Excel $game)
    {
        $data = $game->delete();
        return $data;
    }

    /**
     * RemoveById the specified resource from storage.
     *
     * @param  $id
     * @return bool
     */
    public static function deleteById($id)
    {
        $data = Excel::whereId($id)->delete();
        return $data;
    }

    /**
     * update data in storage.
     *
     * @param  Array $data - Updated Data
     * @param  Int $id - Battle Id
     * @return bool
     */
    public static function status(array $data, $id)
    {
        $data = Excel::where('id', $id)->update($data);
        return $data;
    }

    /**
     * Get data for datatable from storage.
     *
     * @return Game with states, countries
     */
    public static function datatable()
    {
        $data = Excel::orderBy('created_at', 'desc');
      
        return $data;
    }
}
