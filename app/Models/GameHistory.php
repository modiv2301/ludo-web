<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GameHistory extends Model
{
    
    use HasFactory, SoftDeletes;
    public $table = 'game_historys';
    protected $fillable = [

        'room_code_id',
        'user_id',
        'game_id',
        'wallet_id'
       
        
    ];

}
