<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Battle extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'total_player',
        'player1_id',
        'player2_id',
        'player3_id',
        'player4_id',
        'player5_id',
        'player6_id',

        'room_no',
        'entry_fee',
        'prize',
        'start_time',
        'status',
        'is_active'
    ];

    public function playerOne()
    {
        return $this->belongsTo(User::class, 'player1_id');
    }

    public function playerTwo()
    {
        return $this->belongsTo(User::class, 'player2_id');
    }

    public function playerThree()
    {
        return $this->belongsTo(User::class, 'player3_id');
    }

    public function playerFour()
    {
        return $this->belongsTo(User::class, 'player4_id');
    }

    public function playerFive()
    {
        return $this->belongsTo(User::class, 'player5_id');
    }

    public function playerSix()
    {
        return $this->belongsTo(User::class, 'player6_id');
    }
}
