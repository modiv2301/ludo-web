<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Excel extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [

        // 'battle_id',
        'excel_file',
        // 'room_code'
        // 'no_battle',
        // 'entry_amount',
        // 'win_amount',
        // 'trending_game'
    ];

}
