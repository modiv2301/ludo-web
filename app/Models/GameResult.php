<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GameResult extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [

        'user_id',
        'battle_id',
        'battle_room_code_id',
        'status',
        'screenshot',
        'admin_status'
        
    ];

}
