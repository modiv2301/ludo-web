<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BattleRoomCode extends Model
{
    use HasFactory, SoftDeletes;
    public $table = 'battle_room_code';
    protected $fillable = [

       'battle_id',
       'room_code',
        'status',
        'use_room'
         
    ];

}
