<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Winning extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'battle_id',
        'user_id',
        'status',
        'screenshot',
        'is_verify',
        'prize_transfer_status',
        'amount',
    ];

    public function battle()
    {
        return $this->belongsTo(Battle::class, 'battle_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
