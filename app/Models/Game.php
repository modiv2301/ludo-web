<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Game extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [

        'game_name',
        'banner_image',
        'no_player',
        'no_battle',
        'entry_amount',
        'win_amount',
        'trending_game'
    ];

    // public function playerOne()
    // {
    //     return $this->belongsTo(User::class, 'player1_id');
    // }

    // public function playerTwo()
    // {
    //     return $this->belongsTo(User::class, 'player2_id');
    // }

    // public function playerThree()
    // {
    //     return $this->belongsTo(User::class, 'player3_id');
    // }

    // public function playerFour()
    // {
    //     return $this->belongsTo(User::class, 'player4_id');
    // }

    // public function playerFive()
    // {
    //     return $this->belongsTo(User::class, 'player5_id');
    // }

    // public function playerSix()
    // {
    //     return $this->belongsTo(User::class, 'player6_id');
    // }
}
